import * as vkbeautify from 'vkbeautify'; //se importa con npm install -S vkbeautify
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'xml'})
export class XmlPipe implements PipeTransform {

    transform(value: any): string {
        return vkbeautify.xml(value);
    }
}