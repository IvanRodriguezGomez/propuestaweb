import { Observable, of, throwError } from 'rxjs';
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { LoginService } from '../../services/core/login.service';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  public token = '';
  private loginService: LoginService | undefined;

  constructor(private injector: Injector , private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authHeader = this.injector.get(LoginService);
    if (authHeader.session) {
      if (this.lastUrl(req.url) !== 'login') {
        const authReq = req.clone({ headers: req.headers.set( 'Authorization', 'Bearer ' + `${authHeader.session.token}`) });
        return next.handle(authReq);
      }
    }


    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {

          if (event instanceof HttpResponse) {
            console.log('Peticion desde ', req.url,
            ' Estado', event.status,
            ' cuerpo peticion ', event.body);
          }
      }),
      catchError(response => {
          if (response instanceof HttpErrorResponse) {
              if (response.status === 401) {
                console.log('Error de inicio de sesion', response);
                this.router.navigate(['']);
              } else {
                console.log('Peticion desde ', response.url,
                ' Estado', response.status,
                ' cuerpo peticion ', response.message);
              }
          }
          return throwError(response);
      })
  );





  }

  lastUrl(cad: string): string {
    return cad.substr(cad.lastIndexOf('/') + 1);
  }
}
