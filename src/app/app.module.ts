import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';

import { SharedPrimeNgModuleModule } from './modules/shared/shared-prime-ng-module.module';
import { CoreModule } from './modules/core/coremodule.module';
// componente principal de la aplicacion

import { RegistroService } from './services/registro/registro.service';


import { ReCaptchaComponent } from './register/angular2-recaptcha';
import {PricipalComponent} from "./components/pricipal/pricipal.component";
import {MegaMenuModule} from "primeng/megamenu";

@NgModule({
  declarations: [
    AppComponent,
    ReCaptchaComponent,
    PricipalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule, // directivas
    FormsModule, // binding
    SharedPrimeNgModuleModule, // modulo compartido con los componentes de primeng importante que este despues de el BrowserModule
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CoreModule,
    MegaMenuModule
  ],
  providers: [RegistroService],
  bootstrap: [AppComponent]
})
export class AppModule { }
