import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PricipalComponent} from "./components/pricipal/pricipal.component";


const appRoutes: Routes = [
  { path: '', component: PricipalComponent },
  { path: 'home', component: PricipalComponent }
];

// export const AppRoutingModule = RouterModule.forRoot(appRoutes);



@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes, { useHash: true }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule { }


