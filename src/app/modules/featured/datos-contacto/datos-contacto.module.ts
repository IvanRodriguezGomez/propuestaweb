import { SharedPrimeNgModuleModule } from './../../shared/shared-prime-ng-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DatosContactoRoutingModule } from './datos-contacto-routing.module';




@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DatosContactoRoutingModule,
    SharedPrimeNgModuleModule,
  ]
})
export class DatosContactoModule { }
