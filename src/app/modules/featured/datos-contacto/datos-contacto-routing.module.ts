import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeaderComponent} from 'src/app/components/header/header.component'



const routes: Routes = [

  {path: '.', component: HeaderComponent,
    children: [
      ] }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatosContactoRoutingModule { }
