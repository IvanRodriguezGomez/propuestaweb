import { NgModule, Optional, SkipSelf  } from '@angular/core';
import { CommonModule } from '@angular/common';


// servicios singleton
import { HttpClientModule , HttpClient} from '@angular/common/http';
import {ClientewebService} from '../../services/core/clienteweb.service';
import {LoginService} from '../../services/core/login.service';



// interceptores
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from '../../interceptors/core/AuthInterceptor';

// gestion respuestas error
import {ErrorService} from '../../services/core/error.service';

// abrir ventanas
import { WindowsrefService} from '../../services/core/windowsref.service';
import { MessageService } from 'primeng/api';
import { ErrorHandler } from '@angular/core';
import { GlobalErrorHandler } from 'src/app/services/core/global-error-handler';




@NgModule({

  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [],
  providers: [
    ClientewebService,
    LoginService,
    HttpClientModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    ErrorService,
    WindowsrefService,
    MessageService,
    {provide: ErrorHandler, useClass: GlobalErrorHandler}
  ]

})
export class CoreModule {


  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'No se puede importar en el CoreModule en otro modulo que no sea el principal');
    }
  }

 }
