import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { TabMenuModule } from 'primeng/tabmenu';
import { PanelModule } from 'primeng/panel';
import { DialogModule } from 'primeng/dialog';
import { SpinnerModule } from 'primeng/spinner';
import { DropdownModule } from 'primeng/dropdown';
import { ToolbarModule } from 'primeng/toolbar';
import { SidebarModule } from 'primeng/sidebar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { CardModule } from 'primeng/card';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import { TreeTableModule } from 'primeng/treetable';
import { TooltipModule } from 'primeng/tooltip';
import { ToastModule } from 'primeng/toast';
import { FileUploadModule } from 'primeng/fileupload';
import { ProgressBarModule } from 'primeng/progressbar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import {StepsModule} from 'primeng/steps';
import {MenuItem} from 'primeng/api';




// para primeng se exportan modulos,  por tanto lo importaremos en el modulo en el que lo necesitemos y
// luegio importamos en los componentes los componentes de estos modulos
// esto no hace falta si hacemos un shared module que directamente exporta componentes

@NgModule({
  imports: [CommonModule, MenubarModule, ButtonModule, TabMenuModule, PanelModule, DialogModule, SpinnerModule,
    DropdownModule, ToolbarModule, SidebarModule, PanelMenuModule, TableModule, InputTextModule,
     CardModule, ScrollPanelModule, InputTextareaModule, FieldsetModule, CheckboxModule, CalendarModule,
    MultiSelectModule, TreeTableModule, TooltipModule, ToastModule,
    FileUploadModule, ProgressBarModule, MessagesModule, MessageModule,StepsModule],
  exports: [MenubarModule, ButtonModule, TabMenuModule, PanelModule, DialogModule, SpinnerModule, DropdownModule,
    ToolbarModule, SidebarModule, SidebarModule, PanelMenuModule, TableModule, InputTextModule,
     CardModule, ScrollPanelModule, InputTextareaModule, FieldsetModule, CheckboxModule, CalendarModule,
    MultiSelectModule, TreeTableModule, TooltipModule, ToastModule,
    FileUploadModule, ProgressBarModule, MessagesModule, MessageModule,StepsModule]
})
export class SharedPrimeNgModuleModule { }
