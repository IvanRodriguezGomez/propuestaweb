import { NgModule , CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { XmlPipe} from '../../pipes/XmlPipe';
import { SharedPrimeNgModuleModule } from './shared-prime-ng-module.module';



@NgModule({
  imports: [
    CommonModule,
    SharedPrimeNgModuleModule
  ],
  declarations: [
    XmlPipe,
  ],
  exports: [
    XmlPipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class SharedporticmoduleModule { }


