import { TestBed, inject } from '@angular/core/testing';

import { ClientewebService } from './clienteweb.service';

describe('ClientewebService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientewebService]
    });
  });

  it('should be created', inject([ClientewebService], (service: ClientewebService) => {
    expect(service).toBeTruthy();
  }));
});
