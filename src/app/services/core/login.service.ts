import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse } from '@angular/common/http';
import { map, filter } from 'rxjs/operators';
import { Config } from '../../config/config';
import { Session } from '../../model/session';
import { GeneralRequest } from '../../model/request/general-request';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};

@Injectable()
export class LoginService {

    private url = Config.getEnvironmentVariable();
    private loginUrl = this.url + '/services/rest/login';
    private entornoUrl = this.url + '/services/rest/obtenerEntorno';

    esLogin = false;
    session!: Session;

    constructor(private http: HttpClient) { }

    login(request: GeneralRequest): Observable<Session> {


        console.log('path' + this.loginUrl);

        return this.http.post<Session>(this.loginUrl, JSON.stringify(request), httpOptions);

    }


    setSession(session: Session) {
        if (session != null) {
            if (session.companyId != null) {
                localStorage.setItem('pa_session_CompanyId', session.companyId.toString());
            } else {
                localStorage.removeItem('pa_session_CompanyId')
            }
            if (session.companyName != null) {
                localStorage.setItem('pa_session_CompanyName', session.companyName);
            } else {
                localStorage.removeItem('pa_session_CompanyName');
            }
            localStorage.setItem('pa_session_email', session.email);

            if (session.recordnumber != null) {
                localStorage.setItem('pa_session_recordnumber', session.recordnumber.toString());
            } else {
                localStorage.removeItem('pa_session_recordnumber');
            }

            localStorage.setItem('pa_session_role', session.role);

            localStorage.setItem('pa_session_token', session.token);

            if (session.userId != null) {
                localStorage.setItem('pa_session_userId', session.userId.toString());
            } else {
                localStorage.removeItem('pa_session_userId');
            }
            localStorage.setItem('pa_session_userName', session.userName);
            localStorage.setItem('pa_session_userEmail', session.email);
        }
        this.session = session;
    }

    setUsuario(usuario: string) {
        localStorage.setItem('pa_usuario', usuario);
    }

    getUsuario(): any {
       localStorage.getItem('pa_usuario');
  }

  getUsuarioIdString(): any {
      return localStorage.getItem('pa_session_userId');
  }

  getUsuarioId(): number {
      if (localStorage.getItem('pa_session_userId') != null) {
          return Number(localStorage.getItem('pa_session_userId'));
      } else {
          return 0;
      }
  }

  getCompanyId(): number {
      if (localStorage.getItem('pa_session_CompanyId') != null) {
          return Number(localStorage.getItem('pa_session_CompanyId'));
      } else {
          return 0;
      }
  }

  getUsuarioNombre(): any {
      return localStorage.getItem('pa_session_userName');
  }

  getUsuarioEmail(): any {
      return localStorage.getItem('pa_session_userEmail');
  }

  getRole(): any {
      return localStorage.getItem('pa_session_role');
  }

  getToken(): any {
      return localStorage.getItem('pa_session_token');
  }

  close() {
      this.esLogin = false;
      this.session = null as any;
      localStorage.removeItem('pa_session_CompanyId');
      localStorage.removeItem('pa_session_CompanyName');
      localStorage.removeItem('pa_session_email');
      localStorage.removeItem('pa_session_recordnumber');
      localStorage.removeItem('pa_session_role');
      localStorage.removeItem('pa_session_token');
      localStorage.removeItem('pa_session_userId');
      localStorage.removeItem('pa_session_userName');
      localStorage.removeItem('pa_session_userEmail');
      localStorage.removeItem('urlfiltros');
      localStorage.removeItem('faultNoteSearch_textsearch');
      localStorage.removeItem('faultNoteSearch_showOpen');
      localStorage.removeItem('faultNoteSearch_showRead');
      localStorage.removeItem('faultNoteSearch_showProcessed');
      localStorage.removeItem('faultNoteSearch_showClosed');
      localStorage.removeItem('faultNoteSearch_companyId');
      localStorage.removeItem('faultNoteSearch_companyName');
      localStorage.removeItem('faultNoteSearch_userId');
      localStorage.removeItem('faultNoteSearch_userName');
      localStorage.removeItem('faultNoteSearch_period');
      localStorage.removeItem('faultNoteSearch_faultNoteType');
      localStorage.removeItem('faultNoteSearch_faultNoteApplication');
  }



    getEntorno(request: GeneralRequest, token: string) {
        return this.http.post(this.entornoUrl, JSON.stringify(request), httpOptions);
    }

}
