import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../config/config';
import { HttpHeaders } from '@angular/common/http';
import { GeneralRequest } from '../../model/request/general-request';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable()
export class ClientewebService {



  constructor(private http: HttpClient) {

  }

  private url = Config.getEnvironmentVariable();

  urlPersistirRegistroEntrada: string = this.url + '/services/rest/registrarentrada';




  persistirRegistroEntrada(request: GeneralRequest, token: string){
    return this.http.post(this.urlPersistirRegistroEntrada,JSON.stringify(request), httpOptions);
  }




}
