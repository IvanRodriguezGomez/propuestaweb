import { TestBed } from '@angular/core/testing';

import { WindowsrefService } from './windowsref.service';

describe('WindowsrefService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WindowsrefService = TestBed.get(WindowsrefService);
    expect(service).toBeTruthy();
  });
});
