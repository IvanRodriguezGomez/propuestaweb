import { Injectable } from '@angular/core';
import { ErrorMessage } from '../../model/responses/ErrorMessage';
import {HttpErrorResponse} from '@angular/common/http';


@Injectable()
export class ErrorService {

  constructor() { }


  getError(err: any): ErrorMessage {

    const error: ErrorMessage = new ErrorMessage();


      if (err.error != null && err.error.developerMessage !== undefined) { // error devuelto desde el sitema

        error.status = err.error.status;
        error.code = error.code;
        error.message = err.error.message;
        error.developerMessage = err.error.developerMessage;
        error.link = err.error.url;

     } else {

        // si no nos llega error del servidor, no desde la parte Java
        error.code = err.status;
        error.message = err.statusText;
        error.developerMessage = err.message;
        error.link = err.url;

     }

    return error;
   }


}




