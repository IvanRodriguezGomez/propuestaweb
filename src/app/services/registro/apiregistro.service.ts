import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DatosEmpresa } from 'src/app/model/WC/datos-empresa';
import { DatosEmpPublico } from '../../model/WC/datos-emp-publico';
import { DatosContacto } from 'src/app/model/WC/datos-contacto';
import { DatosFacturacion } from 'src/app/model/WC/datos-facturacion';
import { DatosTecnico } from 'src/app/model/WC/datos-tecnico';
import { DatosRegistro } from 'src/app/model/WC/datos-registro';
import { RegistroService } from './registro.service';
import { Config } from 'src/app/config/config';
 
@Injectable({providedIn:'root'})
export class ApiService {

  baseURL = Config.getEnvironmentVariable();
  urlAddEmpresa: string;
 
  empresa:DatosRegistro;
 
  constructor(private http: HttpClient) {
    this.empresa=new DatosRegistro();
  }
 
 
  addEmpresa(regservicio:RegistroService): Observable<any> {


    this.urlAddEmpresa = this.baseURL + '/registro-api/EnviarEmailRegistro';

    
   // this.urlAddEmpresa = 'http://localhost:8080/registro-api/EnviarEmailRegistro';

    console.log('addEmpresa:'+regservicio.datosempresa.paqueteempresa);

    this.empresa.datosEmpresa=regservicio.datosempresa;
    this.empresa.datosEmpresaPublico=regservicio.datosemppublico;
    this.empresa.datosTecnico=regservicio.datostecnico;
    this.empresa.datosContacto=regservicio.datoscontacto;
    this.empresa.datosFacturacion=regservicio.datosfacturacion;
    
    const headers = { 'content-type': 'application/json'}  

    const body=JSON.stringify(this.empresa);
    
    console.log(body);
    
    return this.http.post(this.urlAddEmpresa,body,{'headers':headers});

    
  }

  
 
}