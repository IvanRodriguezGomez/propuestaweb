export class ErrorMessage{

    status!: number;
    code!: number;
    message!: string;
    link!: string;
    developerMessage!: string;

}
