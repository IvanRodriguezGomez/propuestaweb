export class Session {
    token!: string;
    userId!: number;
    userName!: string;
    companyId!: number;
    companyName!: string;
    role!: string;
    email!: string;
    recordnumber!: number;
}

