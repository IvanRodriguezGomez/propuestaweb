export class Config {

    public static getEnvironmentVariable(): string {
        let environment: string;
        environment = window.location.hostname;
        let endPoint = 'localhost';
        switch (environment) {

            case 'localhost':
                endPoint = 'http://10.103.1.40:30153';
                break;
            case '10.120.1.182':
                endPoint = 'http://10.103.1.40:30153';
                break;
            case 'reingtest.portic.net':
                endPoint = 'http://reingtest.portic.net';
                break;
            case 'appdemo.portic.net':
                endPoint = 'http://10.103.1.40:30153';
                break;
            case 'app.portic.net':
                endPoint = 'http://app.portic.net';
                break;
            case '10.120.1.171':
                endPoint = 'http://10.120.1.171:12500';
                break;
            case '10.120.1.172':
                endPoint = 'http://10.120.1.172:12100';
                break;
            case '10.120.1.173':
                endPoint = 'http://10.120.1.173:12200';
                break;
            case '10.120.1.177':
                endPoint = 'http://10.120.1.177:12600';
                break;
            case '10.120.1.178':
                endPoint = 'http://10.120.1.178:12300';
                break;
            case '10.120.1.179':
                endPoint = 'http://10.120.1.179:12400/pasarelaWeb';
                break;
            case '10.120.1.176':
                endPoint = 'http://10.103.1.40:30153';
                break;
            case '10.120.1.182':
                endPoint = 'http://10.120.1.182:12100/pasarelaWeb';
                break;
            default:
                endPoint = 'http://app.portic.net/pasarelaWeb';
                }

        return endPoint;
      }
}
